package fastfood

import akka.actor.testkit.typed.Effect
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit, TestInbox}

import fastfood.Clock.{Schedule, Start}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._

// extends AnyFlatSpec
class ClockSpec
    extends AnyWordSpec  {

  val testKit: ActorTestKit = ActorTestKit("myKit")

  "Clock" should {
    "Send messages to Spawners and to itself" in {
      // val testClock = testKit.spawn(Clock(ClockConfig(0)), "clock")
      // testClock ! Clock.Start

      val testCS = testKit.createTestProbe[Spawner.Command]()
      val testWS = testKit.createTestProbe[Spawner.Command]()

      val testClock = BehaviorTestKit(Clock(ClockConfig(0), testCS.ref, testWS.ref), "clock")
      val clockInbox = testClock.selfInbox()

      testClock.run(Clock.Start)
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.Opening)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.Opening)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.Opening)))

      testClock.run(Clock.Schedule(Time.Opening))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.Breakfast)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.Breakfast)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.Breakfast)))

      testClock.run(Clock.Schedule(Time.Breakfast))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.Lunch)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.Lunch)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.Lunch)))

      testClock.run(Clock.Schedule(Time.Lunch))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.Afternoon)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.Afternoon)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.Afternoon)))

      testClock.run(Clock.Schedule(Time.Afternoon))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.Evening)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.Evening)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.Evening)))

      testClock.run(Clock.Schedule(Time.Evening))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.DayShiftEnd)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.DayShiftEnd)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.DayShiftEnd)))

      testClock.run(Clock.Schedule(Time.DayShiftEnd))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.NightShiftEnd)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.NightShiftEnd)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.NightShiftEnd)))

      testClock.run(Clock.Schedule(Time.NightShiftEnd))
      testClock.expectEffect(Effect.Scheduled(0.second, testClock.ref, Clock.Schedule(Time.Closure)))
      testClock.expectEffect(Effect.Scheduled(0.second, testCS.ref, Spawner.TimeUpdate(Time.Closure)))
      testClock.expectEffect(Effect.Scheduled(0.second, testWS.ref, Spawner.TimeUpdate(Time.Closure)))
    }
  }
}

package fastfood

import akka.actor.testkit.typed.javadsl.BehaviorTestKit
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import fastfood.OrderSystem.{FrontCounterQueueUpdated, GetSmallestFrontCounterQueue, NewFrontCounter, OrderCooked}
import org.scalatest.wordspec.AnyWordSpec

class OrderSystemSpec extends AnyWordSpec  {
  val testKit: ActorTestKit = ActorTestKit("myKit")

  "OrderSystem" should {
    "" in {

    }

    "Report with Smallest Queue" in {
      val testSystem = BehaviorTestKit.create(OrderSystem())

      val testCashier1 = testKit.createTestProbe[Cashier.Command]()
      val testCashier2 = testKit.createTestProbe[Cashier.Command]()
      val testCustomer = testKit.createTestProbe[Customer.Command]()

      testSystem.run(NewFrontCounter(testCashier1.ref))
      testSystem.run(NewFrontCounter(testCashier2.ref))
      // Shuffle queues a bit
      testSystem.run(FrontCounterQueueUpdated(testCashier1.ref, 3))
      testSystem.run(FrontCounterQueueUpdated(testCashier2.ref, 5))
      testSystem.run(FrontCounterQueueUpdated(testCashier1.ref, 7))

      testSystem.run(GetSmallestFrontCounterQueue(testCustomer.ref))

      testCustomer.expectMessage(Customer.SmallestQueue(testCashier2.ref))

    }

    "Send cooked orders to the customer" in {
      val testCustomer = testKit.createTestProbe[Customer.Command]()
      val testSystem = BehaviorTestKit.create(OrderSystem())
      val testOrder = Order(List.empty)
      testSystem.run(OrderSystem.NewOrder(testOrder, testCustomer.ref))
      testSystem.run(OrderCooked(0))
      testCustomer.expectMessage(Customer.Eat(testOrder))
    }

  }

}

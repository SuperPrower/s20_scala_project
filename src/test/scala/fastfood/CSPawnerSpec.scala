package fastfood

import akka.actor.testkit.typed.Effect
import akka.actor.testkit.typed.scaladsl.BehaviorTestKit
import akka.actor.testkit.typed.scaladsl.ActorTestKit
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.util.Random

class CSPawnerSpec extends AnyWordSpec with Matchers {
  val testKit: ActorTestKit = ActorTestKit("myKit")

  val customerConfig = CustomersConfig(
    CustomerConfig(
      RandomizedTime(0, 0)
    ),
    PedestrianCustomersConfig(
      1, 1, 1, 1
    ),
    DriveThruCustomersConfig(
      1, 1, 1, 1, 1
    )
  )

  "CustomerSpawner" should {
    "Spawn Customers" in {
      val testOS = testKit.createTestProbe[OrderSystem.Command]()
      val testSpawner = BehaviorTestKit(CustomerSpawner(0, customerConfig, testOS.ref))

      val verifRNG = new Random(0)

      testSpawner.run(Spawner.TimeUpdate(Time.Breakfast))
      // Because Random is created on-site it's impossible to correctly match two objects, I think
      // testSpawner.expectEffect(Effect.Spawned(PedestrianCustomer(verifRNG.nextInt(), customerConfig.customerConfig, testOS.ref), "PCustomer0"))
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "PCustomer0"
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "DCustomer0"
      testSpawner.run(Spawner.TimeUpdate(Time.Lunch))
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "PCustomer1"
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "DCustomer1"
      testSpawner.run(Spawner.TimeUpdate(Time.Afternoon))
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "PCustomer2"
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "DCustomer2"
      testSpawner.run(Spawner.TimeUpdate(Time.Evening))
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "PCustomer3"
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "DCustomer3"
      testSpawner.run(Spawner.TimeUpdate(Time.DayShiftEnd))
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "DCustomer4"
    }
  }
}

package fastfood

import akka.actor.testkit.typed.Effect
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit, TestInbox}
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._
import scala.util.Random

class CookSpec extends AnyWordSpec {
  val testKit: ActorTestKit = ActorTestKit("myKit")

  "Cook" should {
    "Take and Cook orders" in {
      val testOS = testKit.createTestProbe[OrderSystem.Command]()
      val testInbox = TestInbox[Result]()
      val behaviourKit = BehaviorTestKit(Cook(0, RandomizedTime(1, 0), testOS.ref), "Cook")

      behaviourKit.run(Cook.CookOrder(Order(List.empty), testInbox.ref, 0))
      testInbox.expectMessage(Result.Ok)
      behaviourKit.expectEffect(Effect.Scheduled(1.second, behaviourKit.ref, Cook.FinishOrder(0)))
      behaviourKit.run(Cook.FinishOrder(0))
      testOS.expectMessage(OrderSystem.OrderCooked(0))

    }

    "Decline orders" in {
      val testOS = testKit.createTestProbe[OrderSystem.Command]()
      val testInbox = TestInbox[Result]()
      val behaviourKit = BehaviorTestKit(Cook(0, RandomizedTime(1, 0), testOS.ref), "Cook")

      behaviourKit.run(Cook.CookOrder(Order(List.empty), testInbox.ref, 0))
      behaviourKit.run(Cook.CookOrder(Order(List.empty), testInbox.ref, 1))

      testInbox.expectMessage(Result.Ok)
      testInbox.expectMessage(Result.Busy)
    }
  }
}

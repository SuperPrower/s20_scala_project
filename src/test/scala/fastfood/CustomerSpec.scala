package fastfood

import akka.actor.testkit.typed.Effect
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit, TestInbox}
import akka.actor.typed.scaladsl.Behaviors
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.concurrent.duration._
import scala.util.Random


class CustomerSpec extends AnyWordSpec with Matchers {
  val testKit: ActorTestKit = ActorTestKit("myKit")
  val customerConfig = CustomerConfig(
    RandomizedTime(1, 0)
  )

  "Pedestrian Customer" should {
    "Look for a queue" in {
      val testOS = testKit.createTestProbe[OrderSystem.Command]()
      val c = BehaviorTestKit(PedestrianCustomer(0, customerConfig, testOS.ref))

      c.run(Customer.Start)
      testOS.expectMessage(OrderSystem.GetSmallestFrontCounterQueue(c.ref))


    }

    "Make orders" in {
      // val testOS = testKit.createTestProbe[OrderSystem.Command]()
      val testCashier = testKit.createTestProbe[Cashier.Command]()
      val c = BehaviorTestKit(PedestrianCustomer.makeOrder(new Random(0), customerConfig))

      c.run(Customer.MayITakeYourOrderPlease(testCashier.ref))

      val effect = c.retrieveEffect()
      effect match {
        case Effect.Scheduled(delay, target, message) => {
          delay shouldBe 1.second
          target shouldBe testCashier.ref
          message match {
            case Cashier.TakeOrder(target, _) =>
              target shouldBe c.ref
            case _ => fail()
          }
        }
        case _ => fail()
      }
    }

    "Eat then leave" in {
      val c = BehaviorTestKit(PedestrianCustomer.waitToEat())
      c.run(Customer.Eat(Order(List.empty)))
      c.currentBehavior shouldBe Behaviors.stopped
    }
  }
}

package fastfood

import akka.actor.testkit.typed.Effect
import akka.actor.testkit.typed.scaladsl.{ActorTestKit, BehaviorTestKit}
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

import scala.util.Random

class WSPawnerSpec extends AnyWordSpec with Matchers {
  val testKit: ActorTestKit = ActorTestKit("myKit")

  val workerConfig = WorkerConfig(
    nCooks = 1,
    cookingTime = RandomizedTime(0, 0),
    nFrontCounters = 1,
    nDriveThrus = 1
  )

  "WorkerSpawner" should {
    "Spawn Workers" in {
      val testOS = testKit.createTestProbe[OrderSystem.Command]()
      val testSpawner = BehaviorTestKit(WorkerSpawner(0, workerConfig, testOS.ref))

      val verifRNG = new Random(0)

      testSpawner.run(Spawner.TimeUpdate(Time.Opening))
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "FrontCounter0"
      (testSpawner.retrieveEffect() match { case Effect.Spawned(_, name, _) => name }) shouldBe "DriveThru0"

    }
  }
}

package fastfood

import akka.actor.typed.scaladsl.{Behaviors, StashBuffer}
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout

import scala.util.{Failure, Success}
import scala.concurrent.duration._

object Waiter {

  sealed trait Command

  implicit val timeout: Timeout = Timeout(1.second)

  // var buffer: StashBuffer[Waiter.Command] = _

  def apply(orderSystem: ActorRef[OrderSystem.Command]): Behavior[Command] = {
    waitForOrder
  }

  def waitForOrder: Behavior[Command] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case _ => ???
    }
  }
}



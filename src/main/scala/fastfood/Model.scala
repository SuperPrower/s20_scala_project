package fastfood

import akka.actor.typed.ActorRef

import scala.util.Random

/* Food Model */

case class Order(food: List[Food])

case class CustomerOrder(orderId: Int, customer: ActorRef[Customer.Eat], order: Order, completed: Boolean = false)

sealed trait Food

sealed trait Meat
object Meat {
  case object Beef extends Meat
  case object Mutton extends Meat
  case object Chicken extends Meat
}

sealed trait Size
object Size {
  case object Small extends Size
  case object Medium extends Size
  case object Large extends Size
}

sealed trait MainDish extends Food
object MainDish {
  case class Burger(meat: Meat) extends MainDish
  case object Tenders extends MainDish
  case object Sandwich extends MainDish
}

sealed trait SideDish extends Food
object SideDish {
  case class Fries(size: Size) extends SideDish
  case object Salad extends SideDish
}

sealed trait Drink extends Food
object Drink {
  case class Cola(size: Size) extends Drink
  case object Coffee extends Drink
}

case class Meal(md: MainDish, sd: SideDish, drink: Drink) extends Food

/* Time Model */
sealed trait Time
object Time {
  case object Opening     extends Time
  case object Breakfast   extends Time
  case object Lunch       extends Time
  case object Afternoon   extends Time
  case object Evening     extends Time
  case object DayShiftEnd extends Time /* aka night shift start */
  case object NightShiftEnd extends Time
  case object Closure     extends Time
}


/* Result */
sealed trait Result
object Result {
  case object Ok extends Result
  case object Busy extends Result
}

case class RandomizedTime(avg: Int, varia: Int) {
  def getInt(rng: Random): Long = avg + math.round(varia * (rng.nextGaussian() - 0.5f))
}

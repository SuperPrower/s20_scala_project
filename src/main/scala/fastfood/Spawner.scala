package fastfood

trait Spawner

object Spawner {
  sealed trait Command

  case class TimeUpdate(time: Time) extends Command
  case object Start extends Command
  case object Stop extends Command
}

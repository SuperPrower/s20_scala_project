package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.collection.immutable.Queue

trait Cashier
object Cashier {
  sealed trait Command
  case class Enqueue(customer: ActorRef[Customer.Command]) extends Command
  case class TakeOrder(customer: ActorRef[Customer.Command], order: Order) extends Command
  case object WrapUp extends Command
}

package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.{Behaviors, StashBuffer}
import akka.util.Timeout

import scala.collection.immutable.Queue
import scala.concurrent.duration._

object FrontCounter extends Cashier {

  // implicit val timeout: Timeout = Timeout(1.second)

  def apply(os: ActorRef[OrderSystem.Command]): Behavior[Cashier.Command] = {
    Behaviors.withStash(1) { buffer =>
      work(Queue.empty, os, buffer)
    }
  }

  def work(
            customers: Queue[ActorRef[Customer.Command]],
            os: ActorRef[OrderSystem.Command],
            buffer: StashBuffer[Cashier.Command]
          ): Behavior[Cashier.Command]
  = Behaviors.receive { (ctx, msg) =>
    msg match {
      case Cashier.Enqueue(customer) =>
        ctx.log.info(s"Enqueued ${customer}")
        os ! OrderSystem.FrontCounterQueueUpdated(ctx.self, customers.length + 1)
        if (customers.isEmpty)
          customer ! Customer.MayITakeYourOrderPlease(ctx.self)
        work(customers.enqueue(customer), os, buffer)

      case Cashier.TakeOrder(customer, order) =>
        val (nextCustomer, queue) = customers.dequeue
        ctx.log.info(s"Dequeued ${nextCustomer}")
        assert(customer == nextCustomer, "Customer serviced should be in front of the queue")
        os ! OrderSystem.FrontCounterQueueUpdated(ctx.self, customers.length - 1)
        os ! OrderSystem.NewOrder(order, customer)
        if (queue.nonEmpty) {
          queue.head ! Customer.MayITakeYourOrderPlease(ctx.self)
        } else if (buffer.nonEmpty) {
          // We've been asked to wrap-up
          buffer.unstashAll(work(queue, os, buffer))
        }
        work(queue, os, buffer)

      case Cashier.WrapUp => {
        if (customers.isEmpty) {
          os ! OrderSystem.FrontCounterWentHome(ctx.self)
          ctx.log.info(s"Went Home")
          Behaviors.stopped
        } else {
          ctx.log.info(s"Will go home Later")
          buffer.stash(msg)
          work(customers, os, buffer)
        }
      }
    }
  }
}

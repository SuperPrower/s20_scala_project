package fastfood

import akka.actor.typed.ActorSystem
import pureconfig._
import pureconfig.generic.auto._


/*case class CookingTime(avg: Int, varia: Int)
case class SelectingTime(avg: Int, varia: Int)
case class EatingTime(avg: Int, varia: Int)
case class OrderedDishes(avg: Int, varia: Int)
case class KhinkalisInDish(avg: Int, varia: Int)*/

case class CustomerConfig(
  selectingTime: RandomizedTime
)

case class PedestrianCustomersConfig (
  nCustomersMorning: Int,
  nCustomersLunch: Int,
  nCustomersAfternoon: Int,
  nCustomersEvening: Int,
  // eatingTime: RandomizedTime,
)

case class DriveThruCustomersConfig(
  nCustomersMorning: Int,
  nCustomersLunch: Int,
  nCustomersAfternoon: Int,
  nCustomersEvening: Int,
  nCustomersNight: Int,
)

case class CustomersConfig(
  customerConfig: CustomerConfig,
  pedestrianCustomerConfig: PedestrianCustomersConfig,
  driveThruCustomerConfig: DriveThruCustomersConfig
)

case class WorkerConfig(
  nCooks: Int,
  cookingTime: RandomizedTime,
  nFrontCounters: Int,
  nDriveThrus: Int
)

case class ClockConfig(
  periodLength: Int
)

case class CafeConf(
  randomSeed: Int,

  customersConfig: CustomersConfig,
  workerConfig: WorkerConfig,
  clockConfig: ClockConfig
)

object Main extends App {
  var conf = ConfigSource.default.load[CafeConf]
  conf match {
    case Left(value) =>
      print(s"Unable to load config: $value")

    case Right(value) =>
      val system: ActorSystem[Cafe.Command] = ActorSystem(Cafe(value), "Cafe")
      system ! Cafe.Start
  }
}

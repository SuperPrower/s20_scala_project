package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import scala.util.Random
/**
 * @class WorkerSpawner
 *
 * @brief Handles arrival (spawn) and end of shift (termination) of workers
 */
object WorkerSpawner extends Spawner {

  def apply(seed: Int, cfg: WorkerConfig, os: ActorRef[OrderSystem.Command]): Behavior[Spawner.Command] = {
    val rng = new Random(seed)
    run(rng, cfg, os)
  }

  def run(rng: Random, cfg: WorkerConfig, os: ActorRef[OrderSystem.Command]): Behavior[Spawner.Command] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case Spawner.Start => Behaviors.same
      case Spawner.TimeUpdate(time) => time match {
        case Time.Opening =>
          ctx.log.info(s"Opening")
          // NOTE: in case of adding more spawn times see CustomerSpawner for handling indexes
          (0 until cfg.nFrontCounters).foreach(i => {
            os ! OrderSystem.NewFrontCounter(ctx.spawn(FrontCounter(os), s"FrontCounter${i}"))
          })
          (0 until cfg.nDriveThrus).foreach(i =>
            os ! OrderSystem.NewDriveThru(ctx.spawn(DriveThru(os), s"DriveThru${i}"))
          )
          (0 until cfg.nCooks).foreach(i =>
            os ! OrderSystem.NewCook(ctx.spawn(Cook(rng.nextInt(), cfg.cookingTime, os), s"Chef${i}"))
          )
          Behaviors.same
        case Time.DayShiftEnd => {
          ctx.children
            .filter(a => a.path.name.contains("FrontCounter"))
            .foreach(_.asInstanceOf[ActorRef[Cashier.Command]] ! Cashier.WrapUp) // ask front-counters to wrap-up
          Behaviors.same
        }
        case Time.NightShiftEnd => {
          ctx.children
            .filter(a => a.path.name.contains("DriveThru"))
            .foreach(_.asInstanceOf[ActorRef[Cashier.Command]] ! Cashier.WrapUp) // ask drive-thru to wrap-up
          ctx.children
            .filter(a => a.path.name.contains("Chef"))
            .foreach(_.asInstanceOf[ActorRef[Cook.Command]] ! Cook.WrapUp) // ask cooks to wrap-up

          Behaviors.same
        }
        case _ => Behaviors.same
      }
      case Spawner.Stop => {
        // assuming that by now all workers have terminated nicely
        // TODO: check that
        Behaviors.stopped
      }
    }
  }
}

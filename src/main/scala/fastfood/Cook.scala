package fastfood

import akka.actor.typed.scaladsl.{Behaviors, StashBuffer}
import akka.actor.typed.{ActorRef, Behavior}

import scala.concurrent.duration._
import scala.util.Random

object Cook {

  sealed trait Command

  case class CookOrder(order: Order, replyTo: ActorRef[Result], orderId: Int) extends Command
  case class FinishOrder(orderId: Int) extends Command
  case object WrapUp extends Command

  def apply(seed: Int, cookingTime: RandomizedTime, os: ActorRef[OrderSystem.Command]): Behavior[Command] = {
    val rng = new Random(seed)
    Behaviors.withStash(1) { buffer =>
      waitForOrder(rng, cookingTime, os, buffer)
    }
  }

  def waitForOrder(
                    rng: Random,
                    cookingTime: RandomizedTime,
                    os: ActorRef[OrderSystem.Command],
                    buffer: StashBuffer[Cook.Command]
                  ): Behavior[Command]
  = Behaviors.receive { (ctx, msg) =>
    msg match {
      case CookOrder(order, replyTo: ActorRef[Result], orderId) =>
        ctx.log.info(s"Taking order ${orderId}")
        replyTo ! Result.Ok
        // TODO: make something cute with order itself
        ctx.scheduleOnce(cookingTime.getInt(rng).second, ctx.self, FinishOrder(orderId))
        cookOrder(rng, cookingTime, os, buffer)

      case WrapUp =>
        os ! OrderSystem.CookWentHome(ctx.self)
        ctx.log.info(s"Went Home")
        Behaviors.stopped

      case _ => Behaviors.same

    }
  }

  def cookOrder(rng: Random, cookingTime: RandomizedTime,
                os: ActorRef[OrderSystem.Command],
                buffer: StashBuffer[Cook.Command]
               ): Behavior[Command]
  = Behaviors.receive { (ctx, msg) =>
    msg match {
      case CookOrder(_, replyTo: ActorRef[Result], _) =>
        replyTo ! Result.Busy
        Behaviors.same
      case FinishOrder(orderId) =>
        ctx.log.info(s"Finished order ${orderId}")
        os ! OrderSystem.OrderCooked(orderId)
        if (buffer.nonEmpty) {
          buffer.unstashAll(waitForOrder(rng, cookingTime, os, buffer))
        }
        waitForOrder(rng, cookingTime, os, buffer)
      case WrapUp =>
        ctx.log.info(s"Will go home Later")
        buffer.stash(msg)
        Behaviors.same
      case _ => Behaviors.same

    }
  }
}

package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors
import scala.util.Random

/**
 * @class CustomerSpawner
 * @brief Handles arrival (spawn) of Customers
 */
object CustomerSpawner extends Spawner {

  def apply(seed: Int, cfg: CustomersConfig, os: ActorRef[OrderSystem.Command]): Behavior[Spawner.Command] = {
    val rng = new Random(seed)
    run(rng, cfg, os, 0, 0)
  }

  def run(rng: Random, cfg: CustomersConfig, os: ActorRef[OrderSystem.Command], lastPID: Int, lastDID: Int): Behavior[Spawner.Command] = Behaviors.receive { (ctx, msg) =>
    val spawnPedi = (i: Int) => {
      ctx.spawn(PedestrianCustomer(rng.nextInt(), cfg.customerConfig, os), s"PCustomer${i + lastPID}")
    }
    val spawnThru = (i: Int) => {
      ctx.spawn(DriveThruCustomer(rng.nextInt(), cfg.customerConfig, os), s"DCustomer${i + lastDID}")
    }
    msg match {
      case Spawner.Start => ???
      case Spawner.TimeUpdate(time) => time match {
        case Time.Breakfast =>
          (0 until cfg.pedestrianCustomerConfig.nCustomersMorning)
            .map(spawnPedi).foreach(c => c ! Customer.Start)
          (0 until cfg.driveThruCustomerConfig.nCustomersMorning)
            .map(spawnThru).foreach(c => c ! Customer.Start)
          run(rng, cfg, os, lastPID + cfg.pedestrianCustomerConfig.nCustomersMorning, lastDID + cfg.driveThruCustomerConfig.nCustomersMorning)
        case Time.Lunch =>
          (0 until cfg.pedestrianCustomerConfig.nCustomersLunch)
            .map(spawnPedi).foreach(c => c ! Customer.Start)
          (0 until cfg.driveThruCustomerConfig.nCustomersLunch)
            .map(spawnThru).foreach(c => c ! Customer.Start)
          run(rng, cfg, os, lastPID + cfg.pedestrianCustomerConfig.nCustomersLunch, lastDID + cfg.driveThruCustomerConfig.nCustomersLunch)
        case Time.Afternoon =>
          (0 until cfg.pedestrianCustomerConfig.nCustomersAfternoon)
            .map(spawnPedi).foreach(c => c ! Customer.Start)
          (0 until cfg.driveThruCustomerConfig.nCustomersAfternoon)
            .map(spawnThru).foreach(c => c ! Customer.Start)
          run(rng, cfg, os, lastPID + cfg.pedestrianCustomerConfig.nCustomersAfternoon, lastDID + cfg.driveThruCustomerConfig.nCustomersAfternoon)
        case Time.Evening =>
          (0 until cfg.pedestrianCustomerConfig.nCustomersEvening)
            .map(spawnPedi).foreach(c => c ! Customer.Start)
          (0 until cfg.driveThruCustomerConfig.nCustomersEvening)
            .map(spawnThru).foreach(c => c ! Customer.Start)
          run(rng, cfg, os, lastPID + cfg.pedestrianCustomerConfig.nCustomersEvening, lastDID + cfg.driveThruCustomerConfig.nCustomersEvening)
        case Time.DayShiftEnd =>
          (0 until cfg.driveThruCustomerConfig.nCustomersNight)
            .map(spawnThru).foreach(c => c ! Customer.Start)
          run(rng, cfg, os, lastPID + cfg.pedestrianCustomerConfig.nCustomersEvening, lastDID + cfg.driveThruCustomerConfig.nCustomersEvening)
        case _ => Behaviors.same
      }
      case Spawner.Stop => Behaviors.stopped
    }
  }

}

package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.util.Random
import scala.concurrent.duration._

object Cafe {

  sealed trait Command

  case object Start extends Command

  def apply(cafeConf: CafeConf): Behavior[Command] = {
    var rng = new Random(cafeConf.randomSeed)
    run(cafeConf, rng)
  }
  /*
   * Spawn order:
   * Order System
   * Spawners (need Order System to report Workers to)
   * Clock (needs Spawners to send messages to)
   */
  def run(cafeConf: CafeConf, rng: Random): Behavior[Command] = Behaviors.receive { (ctx, msg) =>
    msg match {
      case Start => {
        val os = ctx.spawn(OrderSystem(), "OrderSystem")
        val ws = ctx.spawn(WorkerSpawner(rng.nextInt(), cafeConf.workerConfig, os), "WorkerSpawner")
        val cs = ctx.spawn(CustomerSpawner(rng.nextInt(), cafeConf.customersConfig, os), "CustomerSpawner")
        val clock = ctx.spawn(Clock(cafeConf.clockConfig, cs, ws), "Clock")
        clock ! Clock.Start
        Behaviors.same
      }
      case _ => Behaviors.same
    }
  }
}

package fastfood

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, Behavior}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.util.{Failure, Success}


/**
 * Processes Orders:
 *  - provides a cachier to the customer, takes orders orders from the cachiers
 *  - (Possibility: take order from the customer directly via kiosk)
 *  - assignes ID to orders
 *  - dispatches orders to Cooks, notifies customers about ready orders
 */
object OrderSystem {

  sealed trait Command

  // Worker Spawner reports spawned/terminated workers so that OrderSystem knows who is working
  case class NewFrontCounter(worker: ActorRef[Cashier.Command]) extends Command
  case class NewDriveThru(worker: ActorRef[Cashier.Command]) extends Command
  case class NewCook(cook: ActorRef[Cook.Command]) extends Command

  // Workers notify OrderSystem about their queue size
  case class FrontCounterQueueUpdated(worker: ActorRef[Cashier.Command], size: Int) extends Command
  case class DriveThruQueueUpdated(worker: ActorRef[Cashier.Command], size: Int) extends Command

  // Workers notify OrderSystem about their departure
  case class FrontCounterWentHome(worker: ActorRef[Cashier.Command]) extends Command
  case class DriveThruWentHome(worker: ActorRef[Cashier.Command]) extends Command
  case class CookWentHome(worker: ActorRef[Cook.Command]) extends Command

  // Customer asks OrderSystem about smallest available queue, gets responded with ActorRef
  case class GetSmallestFrontCounterQueue(replyTo: ActorRef[Customer.Command]) extends Command

  // I doubt that there will be more than 1 DriveThru but whatever
  case class GetSmallestDriveThruQueue(replyTo: ActorRef[Customer.Command]) extends Command

  // Place a new order for the Chef
  case class NewOrder(order: Order, customer: ActorRef[Customer.Command]) extends Command

  // System looks at available chefs until one is not busy
  case class LookForChef(orderId: Int, lastChefTried: Int) extends Command

  // Cooks report ready orders to the OrderSystem
  case class OrderCooked(orderId: Int) extends Command

  case object MoveOn extends Command

  implicit val timeout: Timeout = Timeout(1.second)

  def apply(): Behavior[Command] = {
    work(
      Vector.empty[CustomerOrder],
      Map.empty[ActorRef[Cashier.Command], Int],
      Map.empty[ActorRef[Cashier.Command], Int],
      List.empty[ActorRef[Cook.Command]]
    )
  }

  def work(
            orders: Vector[CustomerOrder],
            frontCounters: Map[ActorRef[Cashier.Command], Int],
            driveThrus: Map[ActorRef[Cashier.Command], Int],
            cooks: List[ActorRef[Cook.Command]]
          ): Behavior[Command]
  = Behaviors.receive { (ctx, msg) =>
    msg match {
      case NewFrontCounter(worker) => work(orders, frontCounters + (worker -> 0), driveThrus, cooks)
      case NewDriveThru(worker) => work(orders, frontCounters, driveThrus + (worker -> 0), cooks)
      case NewCook(cook) => work(orders, frontCounters, driveThrus, cooks.appended(cook))

      case FrontCounterWentHome(worker) => work(orders, frontCounters - worker, driveThrus, cooks)
      case DriveThruWentHome(worker) => work(orders, frontCounters, driveThrus - worker, cooks)
      case CookWentHome(cook) => work(orders, frontCounters, driveThrus, cooks.filter(_ != cook))

      case FrontCounterQueueUpdated(worker, size) => work(orders, frontCounters + (worker -> size), driveThrus, cooks)
      case DriveThruQueueUpdated(worker, size) => work(orders, frontCounters, driveThrus + (worker -> size), cooks)
      case GetSmallestFrontCounterQueue(replyTo) =>
        replyTo ! Customer.SmallestQueue(frontCounters.toSeq.minBy(_._2)._1)
        Behaviors.same
      case GetSmallestDriveThruQueue(replyTo) =>
        replyTo ! Customer.SmallestQueue(driveThrus.toSeq.minBy(_._2)._1)
        Behaviors.same
      case NewOrder(order, customer) =>
        ctx.self ! LookForChef(orders.length, 0)
        ctx.log.info(s"Created order ${orders.length} from ${customer.path.name}")
        work(orders.appended(CustomerOrder(orders.length, customer, order)), frontCounters, driveThrus, cooks)

      case LookForChef(orderId, lastChefTried) =>
        val cook = cooks(lastChefTried)
        ctx.ask(cook, (ref: ActorRef[Result]) => Cook.CookOrder(orders(orderId).order, ref, orderId)) {
          case Success(Result.Ok) =>
            ctx.log.info(s"Chef ${lastChefTried} accepted order with id ${orderId}")
            MoveOn
          case Success(Result.Busy) =>
              if ((lastChefTried + 1) == cooks.length) LookForChef(orderId, 0)
              else LookForChef(orderId, lastChefTried + 1)
          case Failure(exception) =>
            println(exception)
            MoveOn
        }
        Behaviors.same

      case OrderCooked(orderId) =>
        orders(orderId).customer ! Customer.Eat(orders(orderId).order)
        work(orders.updated(orderId, orders(orderId).copy(completed = true)), frontCounters, driveThrus, cooks)

      case MoveOn => Behaviors.same
    }

  }


}

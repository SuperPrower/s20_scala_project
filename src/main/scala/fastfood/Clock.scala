package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._

/**
 * Controls the flow of the times of the day.
 */
object Clock {

  sealed trait Command
  case object Start extends Command
  case class Schedule(t: Time) extends Command

  def apply(cfg: ClockConfig,
            cs: ActorRef[Spawner.Command],
            ws: ActorRef[Spawner.Command]): Behavior[Command]
  = {
    run(cfg, cs, ws)
  }

  def run(cfg: ClockConfig,
          cs: ActorRef[Spawner.Command],
          ws: ActorRef[Spawner.Command]): Behavior[Command]
  = Behaviors.receive { (ctx, msg) =>
    msg match {
      case Start => {
        ctx.scheduleOnce(0.second, ctx.self, Schedule(Time.Opening))
        ctx.scheduleOnce(0.second, cs, Spawner.TimeUpdate(Time.Opening))
        ctx.scheduleOnce(0.second, ws, Spawner.TimeUpdate(Time.Opening))
        ctx.log.info(s"Log: Created Clock")
        Behaviors.same
      }
      case Schedule(Time.Closure) => {
        ctx.log.info(s"Clock stopped")
        cs ! Spawner.Stop
        ws ! Spawner.Stop
        Behaviors.stopped
      }
      case Schedule(t) => {
        ctx.log.info(s"== Now is ${t} ==")

        val newTime: Time = t match {
          case Time.Opening => Time.Breakfast
          case Time.Breakfast => Time.Lunch
          case Time.Lunch => Time.Afternoon
          case Time.Afternoon => Time.Evening
          case Time.Evening => Time.DayShiftEnd
          case Time.DayShiftEnd => Time.NightShiftEnd
          case Time.NightShiftEnd => Time.Closure
        }

        ctx.scheduleOnce(cfg.periodLength.second, ctx.self, Schedule(newTime))
        ctx.scheduleOnce(cfg.periodLength.second, cs, Spawner.TimeUpdate(newTime))
        ctx.scheduleOnce(cfg.periodLength.second, ws, Spawner.TimeUpdate(newTime))
        Behaviors.same
      }
    }
  }


}

package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.util.Random

object DriveThruCustomer extends Customer {

  def apply(seed: Int, cfg: CustomerConfig, os: ActorRef[OrderSystem.Command]): Behavior[Customer.Command] = {
    val rng = new Random(seed)
    driveIn(rng, cfg, os)
  }

  def driveIn(rng: Random, cfg: CustomerConfig, os: ActorRef[OrderSystem.Command]): Behavior[Customer.Command] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case Customer.Start =>
          ctx.log.info(s"Drove In")
          os ! OrderSystem.GetSmallestDriveThruQueue(ctx.self)
          takeQueue(rng, cfg)

        case _ => Behaviors.same
      }
    }
}

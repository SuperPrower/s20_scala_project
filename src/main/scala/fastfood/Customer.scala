package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.util.Random
import scala.concurrent.duration._

// trait Customer
trait Customer {
  /*
   * 1. Customer Walks In / Drives Thru
   * 2. Customer looks for smallest queue (asks the OrderSystem)
   * 3. Customer enqueues himself (msgs FrontCounter worker) and waits servicing
   * 4. Customer generates order and orders it
   * 5. Customer waits to receive order, eats it and leaves
   */

  def takeQueue(rng: Random, cfg: CustomerConfig): Behavior[Customer.Command] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case Customer.SmallestQueue(waiter) =>
          waiter ! Cashier.Enqueue(ctx.self)
          makeOrder(rng, cfg)
        case _ => Behaviors.same
      }
    }
  
  // def makeOrder(rng: Random, cfg: CustomerConfig): Behavior[Customer.Command]
  // same ordering logic for now
  def makeOrder(rng: Random, cfg: CustomerConfig): Behavior[Customer.Command] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case Customer.MayITakeYourOrderPlease(cashier) =>
          ctx.log.info(s"Got to Order")
          // Generate Order
          val order = Order(List.empty)
          val nMeals = rng.nextInt(4)
          // val meals = (0 to nMeals).map(_ => )
          // Make an Order
          ctx.scheduleOnce(cfg.selectingTime.getInt(rng).second, cashier, Cashier.TakeOrder(ctx.self, order))
          waitToEat()
        case _ => Behaviors.same
      }
    }

  def waitToEat(): Behavior[Customer.Command] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case Customer.Eat(_) =>
          ctx.log.info(s"Eating")
          Behaviors.stopped // TODO: make something more interesting
        case _ => Behaviors.same
      }
    }

}

object Customer {
  sealed trait Command
  case object Start extends Command
  case class SmallestQueue(worker: ActorRef[Cashier.Command]) extends Command
  case class MayITakeYourOrderPlease(replyTo: ActorRef[Cashier.Command]) extends Command
  case class Eat(order: Order) extends Command
}
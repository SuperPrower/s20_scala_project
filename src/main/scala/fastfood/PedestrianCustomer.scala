package fastfood

import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.typed.scaladsl.Behaviors

import scala.concurrent.duration._
import scala.util.Random

object PedestrianCustomer extends Customer {

  def apply(seed: Int, cfg: CustomerConfig, os: ActorRef[OrderSystem.Command]): Behavior[Customer.Command] = {
    val rng = new Random(seed)
    walkIn(rng, cfg, os)
  }

  // I feel like I could have used templates to make up for the difference,
  // but let's call it a possibility to implement different behaviour
  def walkIn(rng: Random, cfg: CustomerConfig, os: ActorRef[OrderSystem.Command]): Behavior[Customer.Command] =
    Behaviors.receive { (ctx, msg) =>
      msg match {
        case Customer.Start =>
          ctx.log.info(s"Walked In")
          os ! OrderSystem.GetSmallestFrontCounterQueue(ctx.self)
          takeQueue(rng, cfg)

        case _ => Behaviors.same
      }
    }


}
